# Usando Minikube

  ## Add current-contexts

  ```
    kubectl config current-context minikube
  ```

  ## Listar los contextos de kubernetes

  ```
    kubectl config get-contexts
  ```

  ```
    kubectl config use-context minikube
  ```

