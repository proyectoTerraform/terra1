# Create a project in the group microapp
resource "gitlab_project" "frontend" {
  name              = "frontend"
  description       = "project frontend"
  namespace_id      = data.gitlab_group.microapp.id
  visibility_level  = "public"
}

resource "gitlab_project" "ms_authentication" {
  name              = "ms-authentication"
  description       = "service ms authentication"
  namespace_id      = data.gitlab_group.microapp.id
  visibility_level  = "public"
}

resource "gitlab_project" "ms_postgres" {
  name              = "ms-ms_postgres"
  description       = "service ms postgres"
  namespace_id      = data.gitlab_group.microapp.id
  visibility_level  = "public"
}

resource "gitlab_group_membership" "group_membership" {
  group_id      = data.gitlab_group.microapp.id
  user_id       = data.gitlab_user.manuel.id
  access_level  = "developer"
}
