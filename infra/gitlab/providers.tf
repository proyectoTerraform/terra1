terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.16.1"
    }
  }
}

provider "gitlab" {
  # Configuration options
  token = var.token_gitlab
}