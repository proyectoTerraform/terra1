resource "kubernetes_service_v1" "jenkins-service" {
  metadata {
    namespace = var.namespace
    name      = "jenkins-service"
  }
  spec {
    selector = {
      app = "jenkins"
    }
    type = "NodePort"
    #session_affinity = "ClientIP"
    port {
      protocol    = "TCP"
      port        = 8080
      target_port = "http"
    }
  }
}