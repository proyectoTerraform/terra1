# terraform

La carpeta infra/kubernetes va a crear el namespace, un servicio, un StatefulSet para desplegar el jenkins con su respectivo volumen de datos.

## Ejemplo archivo dev.tfvars
```
  namespace = "dev"
```

## iniciar terraform

```
  terraform init
```

```
  terraform plan -var-file="dev.tfvars"
```

```
  terraform apply -var-file="dev.tfvars"
```

# Hacer el port-forward al servicio
````
  kubectl port-forward --address 0.0.0.0 services/jenkins-service 8081:8080 -n dev
```