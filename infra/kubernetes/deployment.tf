resource "kubernetes_stateful_set_v1" "jenkins" {
  metadata {
    name      = "jenkins"
    namespace = var.namespace
  }
  spec {
    pod_management_policy  = "Parallel"
    service_name = "jenkins-service"
    replicas = 1
    selector {
      match_labels = {
        app = "jenkins"
      }
    }
    template {
      metadata {
        labels = {
          app = "jenkins"
        }
      }
      spec {
        #service_account_name = "jenkins-service"
        container {
          image = "jenkins/jenkins:lts"
          name = "jenkins-container"
          port {
            name = "http"
            container_port = 8080
            protocol = "TCP"
          }

          resources {
            limits = {
              cpu    = "200m"
              memory = "1000Mi"
            }

            requests = {
              cpu    = "200m"
              memory = "1000Mi"
            }
          }

          image_pull_policy = "Always"
          env {
            name  = "JAVA_OPTS"
            value = "-Djenkins.install.runSetupWizard=false"
          }
          volume_mount {
            name = "jenkins-data"
            mount_path = "/var/jenkins_home"
          }
        }
      }
    }
    volume_claim_template {
      metadata {
        name = "jenkins-data"
      }
      spec {
        access_modes = [ "ReadWriteOnce" ]
        resources {
          requests = {
            storage = "3Gi"
          }
        }
      }
    }
  }
}